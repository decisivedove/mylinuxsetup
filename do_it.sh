#!/bin/bash
echo 'Make sure you have yay nstalled'
pacman -S base-devel sudo
echo 'Dealing with sudo and opendoas'
rm /etc/sudoers
mv sudoers/sudoers /etc/sudoers
mv sudoers/doas.conf /etc/doas.conf

echo 'Dealing with ulimit for esync in gaming and other purposes'
rm /etc/security/limits.conf
mv ulimit/limits.conf /etc/security/limits.conf

echo 'Adding user'
sudo useradd -m -g users -G wheel,storage,power -s /bin/bash ansh
echo 'Please set a password for ansh'
passwd ansh


echo 'Moving backgrounds folder'
sudo mv backgrounds /home/ansh/backgrounds
sudo chown -R ansh /home/ansh/backgrounds

echo 'Moving .config directory'
sudo mv .config /home/ansh/.config
sudo chown -R ansh /home/ansh/.config

echo 'Moving .xinitrc directory'
sudo mv .xinitrc /home/ansh/.xinitrc
sudo chown -R ansh /home/ansh/.xinitrc


echo 'Time to install packages and then enable important services'
yay -S $(cat Packages/packages)
yay -S connman-ui-git
echo 'Enabling services'
sudo pacman -S connman-runit connman-gtk
sudo ln -s /etc/runit/sv/connmand /etc/runit/runsvdir/default
sudo ln -s /etc/runit/sv/lvm2 /etc/runit/runsvdir/default
sudo ln -s /etc/runit/sv/device-mapper /etc/runit/runsvdir/default
for daemon in acpid alsasound autofs cronie cupsd dbus elogind xdm fuse haveged hdparm smb sshd syslog-ng; do ln -s /etc/runit/sv/$daemon /etc/runit/runsvdir/default; done

echo 'Finished. Success! Now reboot into your new system'
